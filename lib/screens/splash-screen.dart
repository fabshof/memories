import 'dart:async';

import 'package:flutter/material.dart';

import 'package:camera/camera.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'base-screen-state.dart';
import '../helpers/auth.dart';

class SplashScreen extends StatefulWidget {
  @override
  State createState() => _SplashScreenState();
}

class _SplashScreenState extends BaseScreenState<SplashScreen> {

  String _statusText = '';
  bool _isPermissionsGranted = false;
  List<PermissionGroup> _permissions = [
    PermissionGroup.location,
    PermissionGroup.locationAlways,
    PermissionGroup.locationWhenInUse,
    PermissionGroup.camera,
    PermissionGroup.microphone,
  ];

  @override
  void initState() {
    super.initState();

    // First of all, request permissions
    _requestPermissions();

    GoogleSignIn googleSignIn = AuthHelper().googleSignIn;
    googleSignIn.onCurrentUserChanged
      .listen((GoogleSignInAccount account) {
        setState(() {
          AuthHelper().googleSignInAccount = account;
        });
        if (account != null) {
          _handleInit();
        } else {
          _statusText = '';
        }
      })
      .onError((e) {
        print('Error: ${e}');
      });
    try {
      googleSignIn.signInSilently();
    } catch(error) {
      print('Sign in silently error: ${error}');
    }
    _statusText = 'Logging in user...';
  }

  @override
  Widget build(BuildContext context) {

    TextStyle boldWhiteText = TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 20.0);

    Size size = MediaQuery.of(context).size;

    List<Widget> columnChildWidgets = <Widget>[
      Padding(
        padding: EdgeInsets.only(top: size.height * 0.5),
        child: Center(
          child: Text(
            'memories',
            style: boldWhiteText.copyWith(fontSize: 54.0)
          ),
        ),
      )
    ];

    GoogleSignInAccount signInAccount = AuthHelper().googleSignInAccount;
    if (_isPermissionsGranted && signInAccount == null) {
      columnChildWidgets.add(
        RaisedButton(
          onPressed: _handleSignIn,
          color: Colors.deepOrange,
          child: Text('SIGN IN WITH GOOGLE', style: boldWhiteText.copyWith(color: Colors.black),)
        )
      );
    } else if (!_isPermissionsGranted) {
      columnChildWidgets.add(
        Text('Please go to Settings => Apps => memories and grant permissions!', style: boldWhiteText,)
      );
    } else {
      columnChildWidgets.add(
        Text('Hello ${signInAccount.displayName}!', style: boldWhiteText,)
      );
    }

    Widget statusWidget;
    if (signInAccount != null) {
      statusWidget = Positioned.fill(
        top: size.height - 48.0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
            ),
            Text(_statusText, style: boldWhiteText,)
          ],
        ),
      );
    } else {
      statusWidget = Container();
    }

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Image.asset(
            'assets/images/splash-screen.jpg',
            fit: BoxFit.fitHeight,
            height: size.height,
            colorBlendMode: BlendMode.srcOver,
            color: Color.fromARGB(120, 20, 10, 40),
          ),
          Column(
            children: columnChildWidgets
          ),
          statusWidget
        ],
      )
    );
  }

  Future<void> _requestPermissions() async {
    while (!await _getIsAllPermissionsGranted()) {
      Fluttertoast.showToast(
          msg: 'Sorry, all permissions have to be granted!'
      );
      await PermissionHandler().openAppSettings();
    }
    try {
      setState(() {
        _isPermissionsGranted = true;
      });
    } catch(e) {}
  }

  Future<bool> _getIsAllPermissionsGranted() async {
    Map<PermissionGroup, PermissionStatus> statusMap = await PermissionHandler().requestPermissions(_permissions);

    statusMap.forEach((PermissionGroup permission, PermissionStatus status) {
      if (status == PermissionStatus.denied || status == PermissionStatus.restricted) {
        return false;
      }
    });
    return true;
  }

  Future<void> _handleSignIn() async {
    try {
      await AuthHelper().googleSignIn.signIn();
      setState(() {});
    } catch (error) {
      print('Google SignIn Error ${error}');
    }
  }

  Future<void> _handleSignOut() async {
    try {
      GoogleSignIn googleSignIn = AuthHelper().googleSignIn;
      await googleSignIn.signOut();
      setState(() {});
    } catch (error) {
      print(error);
    }
  }

  Future<void>_handleInit() async {
    try {
      _statusText = 'Connecting to database...';
      AuthHelper authHelper = AuthHelper();
      GoogleSignInAuthentication googleAuth = await authHelper.googleSignInAccount.authentication;
      authHelper.firebaseAuth = FirebaseAuth.instance;
      authHelper.firebaseUser = await authHelper.firebaseAuth.signInWithGoogle(
          idToken: googleAuth.idToken,
          accessToken: googleAuth.accessToken
      );

      _statusText = 'Initializing camera...';
      await availableCameras();
      Timer(Duration(seconds: 1), _goToMainMenu);

    } catch (e) {
      print('Error: ${e}');
      _handleSignOut();
    }
  }

  void _goToMainMenu () {
    Navigator.of(context).pushReplacementNamed('/home');
  }
}