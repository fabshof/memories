import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'base-screen-state.dart';

class ImprintScreen extends StatefulWidget {

  @override
  State createState() => _ImprintScreenState();
}

class _ImprintScreenState extends BaseScreenState<ImprintScreen> {

  TextStyle boldText = TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Imprint'),
      ),
      body: Container(
        child: Center(
          child: ListView(
            children: <Widget>[
              ListTile(
                title: Text('Splash screen: Photo by Jon Tyson', style: boldText,),
                subtitle: Text('on Unsplash'),
              ),
              ListTile(
                title: Text('Main Menu screen: Photo by Andrik Langfield', style: boldText,),
                subtitle: Text('on Unsplash'),
              ),
              ListTile(
                title: Text('Coin: Photo by Holger Link', style: boldText,),
                subtitle: Text('on Unsplash'),
              ),
              ListTile(
                title: Text('Iconset: Entypo by Daniel Bruce', style: boldText,),
                subtitle: Text('on FlutterIcon.com'),
              ),
            ],
          )
        ),
      )
    );
  }
}