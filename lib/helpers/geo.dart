import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:latlong/latlong.dart';
import 'package:flutter_map/flutter_map.dart';

/**
 * A class providing location-based methods.
 * Heavily inspired by: https://gist.github.com/zirinisp/e5cf5d9c33cb0bd815993900618eafe0
 */
class GeoHelper {
  static LatLngBounds getBounds({@required LatLng location, int radiusMeters = 2000}) {

    // The distance between latitudinal lines is equal
    const double lengthMeter = 0.0000089982311916; // ~= 1m
    double latOffset = lengthMeter * radiusMeters;
    double latMax = location.latitude + latOffset;
    double latMin = location.latitude - latOffset;

    // Distance between longitudinal lines decreases from equator to pole
    double lngOffset = latOffset * cos(location.latitude * pi / 180.0);
    double lngMax = location.longitude + lngOffset;
    double lngMin = location.longitude - lngOffset;

    LatLng southwest = LatLng(latMin, lngMin);
    LatLng northeast = LatLng(latMax, lngMax);

    return LatLngBounds(southwest, northeast);
  }

  static bool isWithinBounds({@required LatLng location, @required LatLngBounds bounds}) {
    if (location == null || bounds == null) {
      return false;
    }
    return bounds.northEast.longitude > location.longitude && bounds.northEast.latitude > location.latitude &&
        bounds.southWest.longitude < location.longitude && bounds.southWest.latitude < location.latitude;
  }
}