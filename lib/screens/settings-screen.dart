import 'dart:math';
import 'package:async/async.dart';

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:memories/helpers/custom_icons_icons.dart';
import 'package:random_words/random_words.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong/latlong.dart';

import 'base-screen-state.dart';
import '../helpers/database.dart';
import '../helpers/auth.dart';
import '../helpers/util.dart';
import '../models/memory.dart';
import '../models/enums.dart';


class SettingsScreen extends StatefulWidget {
  @override
  State createState() => _SettingsScreenState();
}

class _SettingsScreenState extends BaseScreenState<SettingsScreen> {

  TextStyle boldText = TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold);

  Function _removeAllCallback;
  Function _createDocsCallback;

  bool _showMapsPosition = false;

  @override
  void initState() {
    _removeAllCallback = this._removeEverything;
    _createDocsCallback = this._createNewDocuments;
    _loadPreferences();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> listWidgets = [];
    if (Util.isInDebugMode()) {
      listWidgets.addAll([
        ListTile(title: Text('Debug Options', style: boldText,),),
        ListTile(
          leading: Icon(CustomIcons.doc_add, color: Colors.lightBlue,),
          title: Text('Create Documents', style: boldText,),
          subtitle: Text('Additionally add new memories and favorites.', style: boldText.copyWith(fontSize: 16.0),),
          onTap: () => _showDialog(
            title: 'Create Documents',
            content: 'Add new documents to the memories- and favorites-databases?',
            action: _createDocsCallback
          ),
        ),
        ListTile(
          leading: Icon(CustomIcons.trash, color: Colors.deepOrange,),
          title: Text('Erase database', style: boldText,),
          subtitle: Text('Removes everything!!1!', style: boldText.copyWith(fontSize: 16.0),),
          onTap: () => _showDialog(
            title: 'Erase database',
            content: 'Remove everything from all databases?',
            action: _removeAllCallback
          ),
        ),

        Divider()
      ]);
    }
    listWidgets.addAll([
      ListTile(title: Text('Map Options', style: boldText,),),
      ListTile(
        leading: Checkbox(
            value: _showMapsPosition,
            onChanged: (bool value) {
              _setShowMapPosition(value);
            }
        ),
        title: Text('Map Position', style: boldText,),
        subtitle: Text('Show your position on the map', style: boldText.copyWith(fontSize: 16.0),),
      )
    ]);

    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: Container(
        child: ListView(
          children: listWidgets.toList(),
        ),
      ),
    );
  }

  void _showDialog({String title, String content, Function action}) {
    _removeAllCallback = null;
    _createDocsCallback = null;
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text(title, style: boldText,),
          content: Text(content, style: boldText,),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text('Close', style: boldText,),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('OK', style: boldText,),
              onPressed: () {
                Navigator.of(context).pop();
                action();
              }
            )
          ],
        );
      },
    );
  }

  void _removeEverything() async {
    print('Delete favorites...');
    await DatabaseHelper().deleteFavorites();
    print('Delete memories...');
    await DatabaseHelper().deleteMemories();
    print('Deleted everything...');
    Fluttertoast.showToast(msg: 'Deletion finished!');
    _removeAllCallback = this._removeEverything;
    _createDocsCallback = this._createNewDocuments;
  }

  void _createNewDocuments() async {

    debugPrint('==============\nSTART GENERATION\n=============');

    debugPrint('Fetching location...');
    Position position = await Geolocator().getLastKnownPosition();

    int memoryCount = 0;
    while (memoryCount == 0) {
      memoryCount = Random().nextInt(30);
    }
    debugPrint('Creating ${memoryCount} memories...');

    List<Future<Memory>> requestList = [];
    for (int i = 0; i < memoryCount; i++) {

      String title = generateWordPairs(safeOnly: false).take(5).join(' ');
      DateTime createdAt = DateTime.now();
      String photoUrl = 'https://picsum.photos/200/300/?image=${Random().nextInt(1000)}';

      Random random = Random();
      double maxDistanceLng = 0.0009;
      double maxDistanceLat = 0.0009;
      double latDeviation = (random.nextBool() ? 1 : -1) * random.nextDouble() * maxDistanceLat;
      double lngDeviation = (random.nextBool() ? 1 : -1) * random.nextDouble() * maxDistanceLng;
      LatLng geolocation = LatLng(position.latitude + latDeviation, position.longitude + lngDeviation);
      debugPrint('Created memory "${title}" at position ${geolocation.latitude}:${geolocation.longitude}...');

      requestList.add(
        DatabaseHelper().createMemory(title: title, createdAt: createdAt, photoUrl: photoUrl, geolocation: geolocation)
      );
    }

    Future.wait(requestList)
      .then((List<Memory> values) {
        Fluttertoast.showToast(msg: 'Created ${values.length ?? 0} new memories!');

        List<Future<bool>> favoritesRequestList = [];
        values.forEach((Memory memory) {

          Random random = Random();
          String memoryId = memory.id;
          bool isLikedByUser = random.nextBool();
          debugPrint('The memory is ${isLikedByUser ? 'liked' : 'NOT liked'} by the current user...');

          if (isLikedByUser) {
            favoritesRequestList.add(
                DatabaseHelper().createFavorite(userId: AuthHelper().firebaseUser.uid, memoryId: memoryId)
            );
          }

          int otherFavoritesCount = random.nextInt(40);
          for (int i = 0; i < otherFavoritesCount; i++) {
            String userId = '${generateNoun().take(4).join('')}-${random.nextInt(100)}';
            debugPrint('User "${userId}" likes it...');
            favoritesRequestList.add(
                DatabaseHelper().createFavorite(userId: userId, memoryId: memory.id)
            );
          }
        });
        Future.wait(favoritesRequestList).then((List<bool> results) {
          debugPrint('==============\nEND GENERATION\n=============');
          Fluttertoast.showToast(
              msg: 'Generation finished'
          );
          _removeAllCallback = this._removeEverything;
          _createDocsCallback = this._createNewDocuments;
        });
      });

  }

  Future<void> _loadPreferences() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      _showMapsPosition = preferences.getBool(describeEnum(SharedPreferencesKeys.gmaps_show_position)) ?? false;
    });
  }

  Future<void> _setShowMapPosition(bool value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setBool(describeEnum(SharedPreferencesKeys.gmaps_show_position), value);
    _showMapsPosition = value;
    setState(() {});
  }

}


