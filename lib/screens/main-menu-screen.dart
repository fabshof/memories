import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';

import 'base-screen-state.dart';
import 'splash-screen.dart';
import '../helpers/auth.dart';
import '../helpers/custom_icons_icons.dart';

class MainMenuScreen extends StatefulWidget {
  @override
  State createState() => _MainMenuScreenState();
}

class _MainMenuScreenState extends BaseScreenState<MainMenuScreen> {

  PackageInfo _packageInfo;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    TextStyle boldWhiteText = TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 20.0);

    List<Widget> mainWidgets = <Widget>[
      RaisedButton(
        elevation: 12.0,
        color: Colors.lightBlue,
        onPressed: _goToCameraScreen,
        child: Text(
          'START',
          style: boldWhiteText.copyWith(fontSize: 54.0),
        ),
      ),
      RaisedButton(
        color: Colors.grey,
        onPressed: _goToImprintScreen,
        child: Text('Imprint', style: boldWhiteText,),
      ),
      RaisedButton(
        color: Colors.grey,
        onPressed: _goToSettingsScreen,
        child: Text('Settings', style: boldWhiteText,),
      ),
      FlatButton(
        onPressed: _handleLogout,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 4.0),
              child: Icon(
                CustomIcons.power_outline,
                color: Colors.white,
                size: 16.0,
              ),
            ),
            Text(
              'Logout',
              style: boldWhiteText,
            )
          ],
        ),
      )
    ];

    if (_packageInfo != null) {
      mainWidgets.add(
        Text('(Version: ${_packageInfo.version}, Build: ${_packageInfo.buildNumber})', style: boldWhiteText,)
      );
    } else {
      _fetchPackageInfo();
    }

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Image.asset(
              'assets/images/main-screen.jpg',
            height: size.height,
            fit: BoxFit.fitHeight,
            colorBlendMode: BlendMode.srcOver,
            color: Color.fromARGB(80, 20, 10, 40),
          ),
          Positioned.fill(
            top: size.height * 0.5,
            bottom: size.height * 0.1,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: mainWidgets,
            )
          )
        ],
      )
    );
  }

  Future<void> _fetchPackageInfo() async {
    _packageInfo = await PackageInfo.fromPlatform();
    setState(() {});
  }

  void _goToCameraScreen() {
    Navigator.of(context).pushNamed('/preview');
  }

  void _goToImprintScreen() {
    Navigator.of(context).pushNamed('/imprint');
  }

  void _goToSettingsScreen() {
    Navigator.of(context).pushNamed('/settings');
  }
  
  void _handleLogout() {
    AuthHelper().firebaseAuth.signOut();
    AuthHelper().googleSignIn.disconnect();
//    Remove all routes from the stack and display the splash screen
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (BuildContext context) => SplashScreen()),
        (Route<dynamic> route) => false
    );
  }
}