import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class CustomLogger {

  static void _showInSnackbar(BuildContext context, String message) {
    Scaffold.of(context).showSnackBar(SnackBar(content: Text(message)));
  }

  static void error(BuildContext context, String message, {Exception exception, StackTrace stackTrace}) {
    print('Error: ${message}');
    _showInSnackbar(context, message);
    if (exception != null) {
      print('**********\nError Message: ${exception}');
    }
    if (stackTrace != null) {
      print('\n\nStacktrace:\n${stackTrace}');
    }
  }
}