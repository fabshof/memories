/// Available screen orientations (landscape-right and portrait-up)
enum ScreenOrientation {
  landscape, portrait
}

/// Names of firestore collections
enum FirestoreCollections {
  users, memories, favorites
}

enum SharedPreferencesKeys {
  gmaps_show_position
}