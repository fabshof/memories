import 'package:flutter/material.dart';

import 'dart:async';

import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthHelper {

  static final AuthHelper _authHelper = AuthHelper._internal();
  factory AuthHelper() {
    return _authHelper;
  }
  AuthHelper._internal(): _googleSignIn = GoogleSignIn();

  final GoogleSignIn _googleSignIn;
  GoogleSignIn get googleSignIn => _googleSignIn;

  GoogleSignInAccount googleSignInAccount;
  FirebaseUser firebaseUser;
  FirebaseAuth firebaseAuth;
}