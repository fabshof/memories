import 'dart:math';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image/image.dart' as ImageUtil;
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

import '../models/enums.dart';
import '../models/memory.dart';

import '../helpers/geo.dart';

class DatabaseHelper {
  static final _databaseHelper = DatabaseHelper._internal();

  factory DatabaseHelper() {
    return _databaseHelper;
  }
  DatabaseHelper._internal();

  final CollectionReference _memoriesCollection = Firestore.instance.collection('memories');
  final CollectionReference _favoritesCollection = Firestore.instance.collection('favorites');

  Future<QuerySnapshot> getMemoriesListInArea(LatLng location, int radiusMeters) async {
    LatLngBounds bounds = GeoHelper.getBounds(location: location, radiusMeters: radiusMeters);
    GeoPoint northEastPoint = GeoPoint(bounds.northEast.latitude, bounds.northEast.longitude);
    GeoPoint southWestPoint = GeoPoint(bounds.southWest.latitude, bounds.southWest.longitude);
    return _memoriesCollection
        .where('l', isLessThanOrEqualTo: northEastPoint)
        .where('l', isGreaterThanOrEqualTo: southWestPoint)
        .getDocuments();
  }
  
  Future<QuerySnapshot> getFavoritesByMemoryId(String memoryId) async {
    return _favoritesCollection
        .where('m', isEqualTo: memoryId)
        .getDocuments();
  }

  Future<Memory> createMemory({String title, DateTime createdAt, String photoUrl, LatLng geolocation}) async {
    Map<String, dynamic> mapData = await Firestore.instance
      .runTransaction((Transaction transaction) async {
        final DocumentSnapshot documentSnapshot = await transaction.get(_memoriesCollection.document());
        final Memory memory = Memory(title: title, createdAt: createdAt, photoUrl: photoUrl, geolocation: geolocation);
        Map<String, dynamic> data = memory.toMap();

        await transaction.set(documentSnapshot.reference, data);
        // Add the absent id
        data['id'] = documentSnapshot.documentID;

        return data;
      });
    return Memory.fromMap(mapData);
  }

  Future<void> deleteMemories() async {
    QuerySnapshot querySnapshot = await _memoriesCollection.getDocuments();
    List<DocumentSnapshot> documentsList = querySnapshot.documents ?? [];
    if (documentsList.length > 0) {
      List<Future> requests = [];
      documentsList.forEach((DocumentSnapshot snapshot) async {
        requests.add(
          snapshot.reference.delete()
        );
      });
      Future.wait(requests).catchError((e) { print('******\nERROR\n******\n${e}\n******'); });
    }
  }

  Future<String> uploadImage({DateTime createdAt, String imagePath}) async {
    File image = File(imagePath);
    int randomInt = Random().nextInt(10000);
    final StorageReference storageReference = FirebaseStorage.instance.ref().child('${createdAt.millisecondsSinceEpoch}-${randomInt}');
    final StorageUploadTask storageUploadTask = storageReference.putFile(image);
    return storageUploadTask.onComplete
      .then((StorageTaskSnapshot taskSnapshot) async {
        return await taskSnapshot.ref.getDownloadURL();
      });
  }

  Future<bool> favorizeMemory({String memoryId, String userId, bool isFavorite}) async {
    if (!isFavorite) {
      QuerySnapshot querySnapshot = await _favoritesCollection.where('u', isEqualTo: userId).where('m', isEqualTo: memoryId).getDocuments();
      List<DocumentSnapshot> documentsList = querySnapshot.documents;
      if (documentsList.length > 0) {
        documentsList.forEach((DocumentSnapshot snapshot) async {
          await snapshot.reference.delete();
        });
      }
      return false;
    } else {
      await createFavorite(userId: userId, memoryId: memoryId);
      return true;
    }
  }

  Future<void> deleteFavorites() async {
    QuerySnapshot querySnapshot = await _favoritesCollection.getDocuments();
    List<DocumentSnapshot> documentsList = querySnapshot.documents ?? [];
    if (documentsList.length > 0) {
      List<Future> requests = [];
      documentsList.forEach((DocumentSnapshot snapshot) async {
        requests.add(
            snapshot.reference.delete()
        );
      });
      Future.wait(requests).catchError((e) { print('******\nERROR\n******\n${e}\n******'); });
    }
  }

  Future<bool> createFavorite({String userId, String memoryId}) async {
    Map<String, dynamic> result = await Firestore.instance
      .runTransaction((Transaction transaction) async {
        final DocumentSnapshot documentSnapshot = await transaction.get(_favoritesCollection.document());
        Map<String, dynamic> data = Map();
        data['m'] = memoryId;
        data['u'] = userId;
        await transaction.set(documentSnapshot.reference, data);
        return data;
      });
    return true;
  }

}