import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';

import '../models/enums.dart';

class Util {

  /*
   * Return a value from a map of objects. If the key doesn't exist, return the default value.
   */
  static T getValueFromMap<T>({
      @required Map map,
      @required Object key,
      T defaultValue
  }) {
    if (map.containsKey(key)) {
      return map[key];
    }
    return defaultValue != null ? defaultValue : null;
  }

  /*
   * Return a boolean depending on the current debug-mode-state
   */
  static bool isInDebugMode() {
    bool inDebugMode = false;
    assert(inDebugMode = true);
    return inDebugMode;
  }

  /*
   * Return the direction value (from 0-360) which has an offset due to screen rotation.
   */
  static double calculateDirectionWithScreenOrientation({
    @required double direction,
    @required ScreenOrientation screenOrientation
  }) {
    if (screenOrientation == ScreenOrientation.landscape) {
      direction += 90.0;
      if (direction > 360.0) {
        direction -= 360;
      }
      return direction;
    } else if (screenOrientation == ScreenOrientation.portrait) {
      return direction;
    } else {
      debugPrint('${screenOrientation} currently not supported');
    }
  }

  /*static getLocalPath() async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }*/

  /*static String getDevicePath(String filePath) {
    return '${(getLocalPath())}/${filePath}';
  }*/

  static Future<String> getLocalPath() async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  static Future<File> getLocalFile(String fileName) async {
    final path = await getLocalPath();
    return File('$path/$fileName');
  }

}