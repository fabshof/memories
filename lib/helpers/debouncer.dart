import 'dart:async';

class Debouncer {

  Debouncer({this.duration, this.callback});
  Duration duration;
  Function callback;
  Timer _debounce;

  void start() {
    _debounce?.cancel();
    _debounce = Timer(duration, callback);
  }

  void stop() {
    _debounce?.cancel();
  }

}