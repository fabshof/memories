import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import '../models/enums.dart';

class ScreenOrientationHelper {

  static Future<void> setScreenOrientation(ScreenOrientation screenOrientation) async {
    switch (screenOrientation) {
      case ScreenOrientation.landscape:
        return SystemChrome.setPreferredOrientations([
          DeviceOrientation.landscapeRight,
          DeviceOrientation.landscapeLeft
        ]);
      case ScreenOrientation.portrait:
      default:
        return SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitUp,
          DeviceOrientation.portraitDown,
        ]);
    }
  }

  static Future<void> resetScreenOrientation() async {
    setScreenOrientation(ScreenOrientation.portrait);
  }
}