import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:intl/intl.dart';
import 'package:memories/helpers/custom_icons_icons.dart';

import '../helpers/database.dart';
import '../helpers/auth.dart';

import '../models/memory.dart';

class MemoryOverlay extends ModalRoute<void> {
  @override
  Duration get transitionDuration => Duration(milliseconds: 500);

  @override
  bool get opaque => false;

  @override
  bool get barrierDismissible => false;

  @override
  Color get barrierColor => Colors.black.withOpacity(0.5);

  @override
  String get barrierLabel => null;

  @override
  bool get maintainState => true;


  MemoryOverlay({@required this.memory});

  Memory memory;

  @override
  Widget buildPage(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Material(
          type: MaterialType.transparency,
          child:  _MemoryOverlayWidget(memory: memory,)
      ),
    );
  }
}

class _MemoryOverlayWidget extends StatefulWidget {

  _MemoryOverlayWidget({@required this.memory});
  Memory memory;

  @override
  State createState() => _MemoryOverlayWidgetState();
}

class _MemoryOverlayWidgetState extends State <_MemoryOverlayWidget> {

  bool _isFavorizingInProgress = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    TextStyle boldText = TextStyle(fontWeight: FontWeight.bold, color: Colors.black, fontSize: 20.0);

    Widget imageWidget = CachedNetworkImage(
      imageUrl: widget.memory.photoUrl,
      placeholder: Container(
        color: Colors.grey,
        child: Center(
          child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.white),),
        ),
        width: screenSize.width * 0.5,
        height: screenSize.height * 0.3,
      ),
      errorWidget: Container(
        color: Colors.grey,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(CustomIcons.warning_empty),
              Text('Sorry, the image is not available', style: boldText,)
            ],
          ),
        ),
        width: screenSize.width * 0.5,
        height: screenSize.height * 0.3,
      ),
    );

    Widget imageWrapperContainer = Container(
      constraints: BoxConstraints(maxHeight: 0.75 * screenSize.height),
      child: imageWidget,
    );

    String title = widget.memory.title.isEmpty ? 'No title' : '"${widget.memory.title}"';

    DateFormat dateFormat = DateFormat('yyyy-MM-dd');
    String createdAtString = 'Taken at: ${dateFormat.format(widget.memory.createdAt)}';

    Widget metaInfoWidget = Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Center(
          child: Text(title, style: boldText,),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text(createdAtString, style: boldText,),
            FlatButton(
              onPressed: () async {
                _isFavorizingInProgress = true;
                setState(() {});
                debugPrint('${widget.memory.title} =>  ${widget.memory.id}');
                bool result = await DatabaseHelper().favorizeMemory(memoryId: widget.memory.id, userId: AuthHelper().firebaseUser.uid, isFavorite: !widget.memory.isFavoriteOfUser);
                widget.memory.isFavoriteOfUser = result;
                _isFavorizingInProgress = false;
                setState(() {});
              },
              child: _getFavorizeIcon()
            )
          ],
        )
      ],
    );

    return Center(
        child: Container(
          width: 0.8 * screenSize.width,
          child: Material(
              type: MaterialType.card,
              color: Colors.white,
              child: Padding(
                padding: EdgeInsets.all(6.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          IconButton(
                              onPressed: () => Navigator.pop(context, null),
                              icon: Icon(CustomIcons.cancel_circled_outline, color: Colors.lightBlue,)
                          ),
                        ],
                      ),
                    ),
                    imageWrapperContainer,
                    metaInfoWidget
                  ],
                )
              )
          ),
        )
    );
  }

  Widget _getFavorizeIcon() {
    if (_isFavorizingInProgress) {
      return CircularProgressIndicator(valueColor: AlwaysStoppedAnimation(Colors.deepOrange));
    }
    return Icon((widget.memory.isFavoriteOfUser) ? CustomIcons.heart_filled : CustomIcons.heart, color: Colors.red.withOpacity(0.5),);
  }

  @override
  void dispose() {
    super.dispose();
  }

}