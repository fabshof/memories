import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/foundation.dart';
import 'package:memories/helpers/custom_icons_icons.dart';

class BouncingIconWidget extends StatefulWidget {

  BouncingIconWidget({@required this.icon});
  Icon icon;

  @override
  State createState() => _BouncingIconWidgetState();
}

class _BouncingIconWidgetState extends State<BouncingIconWidget> with SingleTickerProviderStateMixin {

  AnimationController _animationController;
  Animation<double> _sizeAnimation;

  @override
  Widget build(BuildContext context) {
    return Transform(
      alignment: Alignment.center,
      transform: Matrix4.diagonal3Values(
          _sizeAnimation.value,
          _sizeAnimation.value,
          1.0
      ),
      child: widget.icon,
    );
  }

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
        vsync: this,
        duration: Duration(milliseconds: 1000)
    )
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((AnimationStatus status) {
        if (status == AnimationStatus.completed) {
          _animationController.reverse();
        } else if (status == AnimationStatus.dismissed) {
          _animationController.forward();
        }
      });
    _sizeAnimation = Tween(begin: 0.0, end: 1.0)
        .animate(
        CurvedAnimation(
            parent: _animationController,
            curve: Interval(0.0, 0.95, curve: Curves.elasticOut)
        )
    );
    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}