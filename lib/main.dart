import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

import 'screens/main-menu-screen.dart';
import 'screens/preview-screen.dart';
import 'screens/imprint-screen.dart';
import 'screens/settings-screen.dart';
import 'screens/sandbox/sandbox.dart';
import 'screens/splash-screen.dart';

Future<Null> main() async {
  runApp(
      MaterialApp(
        title: 'memories',
        home: SplashScreen(),
        routes: <String, WidgetBuilder> {
          '/home': (BuildContext context) => MainMenuScreen(),
          '/imprint': (BuildContext context) => ImprintScreen(),
          '/preview': (BuildContext context) => PreviewScreen(),
          '/settings': (BuildContext context) => SettingsScreen()
        },
        theme: ThemeData(fontFamily: 'AmaticSC', ),
        debugShowCheckedModeBanner: false,
      )
  );

}