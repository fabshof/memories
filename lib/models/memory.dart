import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:latlong/latlong.dart';

class Memory {

  Memory({
    @required this.title,
    @required this.geolocation,
    @required this.photoUrl,
    @required this.createdAt
  });

  String _id;
  DateTime createdAt;
  LatLng geolocation;
  String photoUrl;
  String title;
  int _numberOfFavorites = 0;
  bool isFavoriteOfUser = false;

  String get id => _id;

  set numberOfFavorites(int number) {
    if (number >= 0) {
      this._numberOfFavorites = number;
    } else {
      this._numberOfFavorites = 0;
    }
  }

  Memory.fromMap(Map<String, dynamic> map) {
    this._id = map['id'];
    this.createdAt = map['c']; // created_at
    this.geolocation = LatLng(map['l'].latitude, map['l'].longitude); // geolocation
    this.photoUrl = map['p']; // photo_url
    this.title = map['t']; // title
  }

  Map<String, dynamic> toMap() {
    Map mapToReturn = Map<String, dynamic>();
    mapToReturn['c'] = this.createdAt;
    mapToReturn['l'] = GeoPoint(this.geolocation.latitude, this.geolocation.longitude);
    mapToReturn['p'] = this.photoUrl;
    mapToReturn['t'] = this.title;

    return mapToReturn;
  }

  int getHealthIndex() {
    DateTime now = DateTime.now();
    return 10 - now.difference(this.createdAt).inDays + this._numberOfFavorites;
  }

}