import 'dart:async';

import 'package:flutter/material.dart';
import '../helpers/screen-orientation-helper.dart';
import '../models/enums.dart';

class BaseScreenState<T> extends State {

  BaseScreenState({
    this.screenOrientation = ScreenOrientation.portrait
  });
  ScreenOrientation screenOrientation;

  @override
  void initState() {
    _setScreenOrientation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return null;
  }


  @override
  void dispose() {
    screenOrientation = ScreenOrientation.portrait;
    _setScreenOrientation();
    super.dispose();
  }

  Future<void> _setScreenOrientation() async {
    await ScreenOrientationHelper.setScreenOrientation(screenOrientation);
  }
}