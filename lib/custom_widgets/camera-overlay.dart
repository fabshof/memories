import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:camera/camera.dart';
import 'package:memories/helpers/custom_icons_icons.dart';
import 'package:path_provider/path_provider.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:image/image.dart' as ImageUtil;
import 'package:flutter_image_compress/flutter_image_compress.dart';

class CameraOverlay extends ModalRoute<Map<String, String>> {
  @override
  Duration get transitionDuration => Duration(milliseconds: 500);

  @override
  bool get opaque => false;

  @override
  bool get barrierDismissible => false;

  @override
  Color get barrierColor => Colors.black.withOpacity(0.5);

  @override
  String get barrierLabel => null;

  @override
  bool get maintainState => true;

  @override
  Widget buildPage(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Material(
        type: MaterialType.transparency,
        child:  _CameraOverlayWidget()
      ),
    );
  }
}

class _CameraOverlayWidget extends StatefulWidget {
  @override
  State createState() => _CameraOverlayWidgetState();
}

class _CameraOverlayWidgetState extends State <_CameraOverlayWidget> {

  CameraController _cameraController;
  List <CameraDescription> _cameras;
  String _imagePath;

  TextEditingController _textEditingController = TextEditingController();
  String _titleString = '';

  @override
  void initState() {
    super.initState();
    _setupCameras();
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    TextStyle boldText = TextStyle(fontWeight: FontWeight.bold, color: Colors.black, fontSize: 20.0);

    List<Widget> mainWidgets = [];
    FloatingActionButton floatingActionButton;

    // Add a button at the top to dismiss the content
    mainWidgets.add(
      Container(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            IconButton(
              onPressed: () => Navigator.pop(context, null),
              icon: Icon(CustomIcons.cancel_circled_outline, color: Colors.lightBlue,)
            )
          ],
        ),
      )
    );

    if (this._cameraController != null && this._cameraController.value.isInitialized) {

      // An image has been taken
      if (this._imagePath != null && this._imagePath.isNotEmpty) {
        mainWidgets.add(
          RotatedBox(
            quarterTurns: 0,
            child: FadeInImage(
              height: screenSize.height * 0.4,
              placeholder: MemoryImage(kTransparentImage),
              image: MemoryImage(File(this._imagePath).readAsBytesSync())
            )
          )
        );
        mainWidgets.add(
            Padding(
              padding: EdgeInsets.only(left: 4.0, right: 4.0),
              child: TextField(
                controller: _textEditingController,
                decoration: const InputDecoration(
                  hintText: 'Write something about your memory...',
                ),
                onChanged: (String value) {
                  _titleString = value;
                  setState(() {});
                },
                style: boldText,
              )
            )
        );
        floatingActionButton = FloatingActionButton(
          backgroundColor: Colors.deepOrange,
          foregroundColor: Colors.white,
          onPressed: () {
            Map<String, String> valuesToReturnMap = Map();
            valuesToReturnMap['imagePath'] = '${this._imagePath}';
            valuesToReturnMap['title'] = _titleString;
            Navigator.pop(context, valuesToReturnMap);
          },
          child: Icon(CustomIcons.ok_outline),
        );

        // Show the camera-preview as no photo was taken
      } else {
        mainWidgets.add(
            AspectRatio(
              aspectRatio: _cameraController.value.aspectRatio,
              child: CameraPreview(_cameraController),
            )
        );
        floatingActionButton = FloatingActionButton(
          backgroundColor: Colors.deepOrange,
          foregroundColor: Colors.white,
          onPressed: () async {
            this._imagePath = await this._takePhoto();
            setState(() {});
          },
          child: Icon(CustomIcons.camera_outline),
        );
      }

      // Additionally, add a padding to the fab
      mainWidgets.add(
        Padding(
          padding: EdgeInsets.only(top: 6.0),
          child: floatingActionButton,
        )
      );
    } else {
      mainWidgets.add(
        Center (
          child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation(Colors.orange),),
        )
      );
    }

    return Center(
        child: Container(
          width: 0.8 * screenSize.width,
          child: Material(
            type: MaterialType.card,
            color: Colors.white,
            child: Padding(
              padding: EdgeInsets.all(6.0),
              child: Container(
                child: ListView(
                  shrinkWrap: true,
                  children: mainWidgets.toList(),
                ),
              )
            )
          ),
        )
    );
  }

  @override
  void dispose() {
    _cameraController?.dispose();
    _textEditingController?.dispose();
    super.dispose();
  }

  Future<void> _setupCameras() async {
    try {
      // initialize cameras.
      _cameras = await availableCameras();
      // initialize camera controllers.
      _cameraController = new CameraController(_cameras[0], ResolutionPreset.medium);
      await _cameraController.initialize();
    } on CameraException catch (e, s) {
      print('CameraException ${e}');
    }
    if (!mounted) {
      return;
    }
    setState(() {});
  }

  Future<String> _takePhoto() async {
    if (!_cameraController.value.isInitialized || _cameraController.value.isTakingPicture) {
      print('Error: Controller not ready yet');
      return null;
    }

    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures/memories';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${DateTime.now().millisecondsSinceEpoch}.jpg';

    try {
      await _cameraController.takePicture(filePath);
    } on CameraException catch (e) {
      print('Error: ${e}');
      return null;
    }

    // Resize the image first
    int resizeHeight = 400; // px
    int encodeQuality = 70; // %

    // Use FlutterImageCompress to resize the image (with quality loss), as it
    // reduces the data amount of the image in an enormous way. A list of integers
    // is returned, which can be passed to the image package directly. If done by
    // the image package itself, the resizing would take ~15s. Now it only takes <5s.
    List<int> fileList = await FlutterImageCompress.compressWithFile(filePath, quality: encodeQuality, minHeight: resizeHeight);
    ImageUtil.Image image = ImageUtil.decodeImage(fileList);

    // Image processing
    image = ImageUtil.sepia(image);
    image = ImageUtil.vignette(image);
    File(filePath).writeAsBytesSync(ImageUtil.encodeJpg(image));

    return filePath;
  }


}