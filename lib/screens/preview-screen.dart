import 'dart:async';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:memories/custom_widgets/bouncing-icon.dart';

import 'package:screen/screen.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:flutter_compass/flutter_compass.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/memory.dart';
import '../models/enums.dart';

import '../helpers/database.dart';
import '../helpers/auth.dart';
import '../helpers/geo.dart';
import '../helpers/custom_icons_icons.dart';

import '../custom_widgets/camera-overlay.dart';
import '../custom_widgets/memory-overlay.dart';

import 'base-screen-state.dart';

class PreviewScreen extends StatefulWidget {
  @override
  State createState() => _PreviewScreenState();
}

class _PreviewScreenState extends BaseScreenState<PreviewScreen> with SingleTickerProviderStateMixin {

  _PreviewScreenState(): super();

  final TextStyle boldText = TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold);
  final double defaultZoom = 18.0;
  final int _smallBoundsRadius = 1000; // m
  final int _bigBoundsRadius = 2000; // m

  MapController _mapController = MapController();
  bool _showMapsPosition = false;
  Position _position;
  double _bearing = 0.0;
  bool _isUploadingMemory = false;
  List<StreamSubscription<dynamic>> _locationStreamSubscriptions = [];
  LatLngBounds _latLngBounds;

  Timer _periodicTimer;

  List<Memory> _memoryItems = [];
  Marker _userPosition;
  List<Marker> _markerList = [];


  @override
  void initState() {
    super.initState();

    //  Make the screen stay awake
    Screen.keepOn(true);

    LocationOptions locationOptions = LocationOptions(accuracy: LocationAccuracy.bestForNavigation, distanceFilter: 0);
    Geolocator geolocator = Geolocator();

    geolocator.getPositionStream(locationOptions).then((Stream<Position> stream) {
      _locationStreamSubscriptions.add(
          stream.listen((Position position) {
            _position = position;
            _mapController.move(LatLng(position.latitude, position.longitude), defaultZoom);
            setState(() {});

            LatLng currentPosition = _getCurrentPosition();
            _animatedMapMove(currentPosition, null);

            if(!GeoHelper.isWithinBounds(location: currentPosition, bounds: _latLngBounds)) {
              _latLngBounds = GeoHelper.getBounds(location: currentPosition, radiusMeters: _smallBoundsRadius);
              _periodicallyPollData();
            }
            setState(() {});
          })
      );
    });

    _locationStreamSubscriptions.add(
        FlutterCompass.events.listen((double bearing) {
          _bearing = bearing;
          setState(() {});
        })
    );
  }

  @override
  void dispose() {
    _locationStreamSubscriptions?.forEach((StreamSubscription subscription) {
      subscription.cancel();
    });
    _periodicTimer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Marker> markers = [];
    if (_userPosition != null) {
      markers.add(_userPosition);
    }
    markers.addAll(_markerList.toList());

    return FutureBuilder(
      future: SharedPreferences.getInstance(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          SharedPreferences sharedPreferences = snapshot.data;
          _showMapsPosition = sharedPreferences.getBool(describeEnum(SharedPreferencesKeys.gmaps_show_position)) ?? false;

          return Scaffold(
              floatingActionButton: FloatingActionButton(
                backgroundColor: (_isUploadingMemory || _position == null) ? Colors.deepOrange.withOpacity(0.5) : Colors.deepOrange,
                child: Icon(CustomIcons.plus_outline, color: Colors.white,),
                onPressed: (_isUploadingMemory || _position == null) ? null :  _openPhotoPopup,
              ),
              body: Container(
                  child: Stack(
                    children: <Widget>[
                      Container(
                        child: Center(
                          child: FlutterMap(
                            mapController: _mapController,
                            options: MapOptions(
                              interactive: true,
                              onPositionChanged: (MapPosition mapPosition, bool isGesture) {
                                _animatedMapMove(_getCurrentPosition(), defaultZoom);
                              },
                              zoom: defaultZoom,
                              minZoom: defaultZoom,
                            ),
                            layers: [
                              TileLayerOptions(
                                urlTemplate: "https://api.mapbox.com/v4/{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}",
                                additionalOptions: {
                                  'accessToken': 'pk.eyJ1IjoiZmFiaGVuIiwiYSI6ImNqcWV6amc0ZDRrMmE0OGp5Zmg5ZmIxbjIifQ.npkWZeL__eblrV2f4nF9XQ',
                                  'id': 'mapbox.light'
                                },
                              ),
                              MarkerLayerOptions(markers: markers)
                            ],
                          ),
                        ),
                      ),
                      Positioned.fill(
                          child: Center(
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Visibility(
                                    visible: _position == null,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                                        color: Colors.black.withOpacity(0.5),
                                      ),
                                      child: Padding(
                                          padding: EdgeInsets.all(4.0),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              BouncingIconWidget(icon: Icon(CustomIcons.globe_outline, color: Colors.white, size: 30.0,),),
                                              Text('Looking for your position ...', style: boldText.copyWith(color: Colors.white, fontSize: 28.0),)
                                            ],
                                          )
                                      ),
                                    )
                                ),
                                Visibility(
                                    visible: _isUploadingMemory,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                                        color: Colors.black.withOpacity(0.5),
                                      ),
                                      child: Padding(
                                        padding: EdgeInsets.all(4.0),
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            BouncingIconWidget(icon: Icon(CustomIcons.upload_cloud_outline, color: Colors.white, size: 30.0,),),
                                            Text('Saving memory ...', style: boldText.copyWith(color: Colors.white, fontSize: 28.0),)
                                          ],
                                        ),
                                      ),
                                    )
                                )
                              ],
                            ),
                          )
                      ),
                    ],
                  )
              )
          );
        }
        return Container();
      },
    );
  }

  void _animatedMapMove(LatLng position, double destZoom) {

    _userPosition = Marker(
      width: 40.0,
      height: 40.0,
      point: _getCurrentPosition(),
//      builder: (ctx) => Container(
      builder: (ctx) => Visibility(
        visible: _showMapsPosition,
        child: Container(
          child: Transform.rotate(
            angle: ((_bearing - 45) / 360) * 2 * pi,
            child: Icon(CustomIcons.direction_outline, color: Colors.black87, size: 40.0,),
          ),
        ),
      )
    );

    _mapController.move(position, _mapController.zoom ?? defaultZoom);
  }

  LatLng _getCurrentPosition() {
    if (_position != null) {
      return LatLng(_position.latitude ?? 0.0, _position.longitude ?? 0.0);
    }
    return LatLng(0.0, 0.0);
  }
  
  void _periodicallyPollData() {
    this._periodicTimer?.cancel();
    this._getData();
    this._periodicTimer = Timer.periodic(Duration(seconds: 10), (Timer timer) {
      this._getData();
    });
  }

  void _getData() {
    DatabaseHelper().getMemoriesListInArea(_getCurrentPosition(), _bigBoundsRadius)
      .then((QuerySnapshot snapshot) {
        List<Memory> memories = snapshot.documents.map((DocumentSnapshot documentSnapshot) {
          documentSnapshot.data['id'] = documentSnapshot.documentID;
          return Memory.fromMap(documentSnapshot.data);
        }).toList();
        this._memoryItems = memories;

        List<Future<QuerySnapshot>> futuresList = [];
        memories.forEach((Memory memory) {
          futuresList.add(DatabaseHelper().getFavoritesByMemoryId(memory.id));
        });
        return Future.wait<QuerySnapshot>(futuresList);
      })
      .then((List<QuerySnapshot> snapshots) {
        snapshots
          .forEach((QuerySnapshot singleFavoriteSnapshot) {
            List<DocumentSnapshot> documents = singleFavoriteSnapshot.documents;
            if (documents.length > 0) {
              DocumentSnapshot firstDocument = documents.first;
              bool isFavoriteOfUser = (documents
                .firstWhere((DocumentSnapshot snapshot) {
                  return snapshot.data['u'] == AuthHelper().firebaseUser.uid;
                }, orElse: () => null)
              ) != null;

              if (firstDocument != null) {
                int memoryToUpdateIndex = this._memoryItems.indexWhere((Memory memory) {
                  return memory.id == firstDocument.data['m'];
                });
                if (memoryToUpdateIndex > -1) {
                  Memory memoryToUpdate = this._memoryItems[memoryToUpdateIndex];
                  memoryToUpdate.numberOfFavorites = documents.length;
                  memoryToUpdate.isFavoriteOfUser = isFavoriteOfUser;
                  this._memoryItems[memoryToUpdateIndex] = memoryToUpdate;
                }
              }
            }
          });
        List<Marker> tmpMarkerList = [];
        _memoryItems.forEach((Memory memory) {
          String title = memory.title.isEmpty ? 'No title' : memory.title;

          int healthIndex = memory.getHealthIndex();
          Color iconColor = Colors.amberAccent.withOpacity(0.8);
          if (healthIndex < 10) {
            iconColor = Colors.brown[200].withOpacity(0.9);
          } else if (healthIndex < 30) {
            iconColor = Colors.grey.withOpacity(0.8);
          }

          tmpMarkerList.add(
            Marker(
              point: memory.geolocation,
              width: 30.0,
              height: 30.0,
              builder: (ctx) => Container(
                child: Tooltip(
                  message: title,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(ctx).push(MemoryOverlay(memory: memory));
                    },
                    child: Icon(CustomIcons.location, color: iconColor,),
                  ),
                ),
              )
            )
          );
        });
        _markerList = tmpMarkerList;
        setState(() {});
      });
  }

  void _openPhotoPopup() async {
    final Map<String, String> resultsMap = await Navigator.of(context).push(CameraOverlay());

    if (resultsMap != null) {
      _periodicTimer?.cancel();
      _isUploadingMemory = true;
      setState(() {});

      DatabaseHelper().createMemory(
          title: resultsMap['title'],
          createdAt: DateTime.now(),
          photoUrl: await DatabaseHelper().uploadImage(createdAt: DateTime.now(), imagePath: resultsMap['imagePath']),
          geolocation: _getCurrentPosition()
      )
        .then((_) {
           _isUploadingMemory = false;
           setState(() {});
           _periodicallyPollData();
        });
    }
  }
}