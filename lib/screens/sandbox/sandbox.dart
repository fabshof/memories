import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter_compass/flutter_compass.dart';
import 'package:memories/helpers/custom_icons_icons.dart';

import '../../helpers/debouncer.dart';
import '../../helpers/util.dart';

class SandBox extends StatefulWidget {

  @override
  State createState() => _SandBoxState();
}

class _SandBoxState extends State<SandBox> with TickerProviderStateMixin  {

  MapController _mapController = MapController();
  Position _position;
  double _bearing = 0.0;
  double zoomLevel = 18.5;

  Debouncer _bearingDebouncer;
  bool _isRotationEnabled = true;

  @override
  void initState() {

    _bearingDebouncer = Debouncer(duration: Duration(milliseconds: 100), callback: () { _isRotationEnabled = true; });

    LocationOptions locationOptions = LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 2);
    Geolocator geolocator = Geolocator();

    geolocator.getPositionStream(locationOptions).then((Stream<Position> stream) {
      stream.listen((Position position) {
        _position = position;
        _animatedMapMove(_getCurrentPosition(), null);
        setState(() {});
      });
    });

    FlutterCompass.events.listen((double bearing) {
      if (_isRotationEnabled) {
        _isRotationEnabled = false;
        _bearing = bearing;
        _bearingDebouncer.start();
        setState(() {});
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    var markers = <Marker>[
      Marker(
        width: 30.0,
        height: 30.0,
        point: _getCurrentPosition(),
        builder: (ctx) => Container(
          child: GestureDetector(
            // Rotate against the icon's default orientation of 45°
            child: Tooltip(
              message: 'This is a message',
              child: Icon(CustomIcons.location, color: Colors.red.withOpacity(0.7),),
            ),
          ),
        ),
      )
    ];

    return FlutterMap(
      mapController: _mapController,
      options: MapOptions(
        center: LatLng(0.0, 0.0),
        interactive: true,
        onPositionChanged: (MapPosition mapPosition, bool isGesture) {
          _animatedMapMove(_getCurrentPosition(), null);
        },
        minZoom: zoomLevel,
      ),
      layers: [
        TileLayerOptions(
          urlTemplate: "https://api.mapbox.com/v4/{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}",
          additionalOptions: {
            'accessToken': 'pk.eyJ1IjoiZmFiaGVuIiwiYSI6ImNqcWV6amc0ZDRrMmE0OGp5Zmg5ZmIxbjIifQ.npkWZeL__eblrV2f4nF9XQ',
            'id': 'mapbox.light'
          },
        ),
        MarkerLayerOptions(markers: markers)
      ],
    );
  }

  LatLng _getCurrentPosition() {
    if (_position != null) {
      return LatLng(_position.latitude ?? 0.0, _position.longitude ?? 0.0);
    }
    return LatLng(0.0, 0.0);
  }

  void _animatedMapMove(LatLng position, double destZoom) {
    final _latTween = Tween<double>(
        begin: _mapController.center.latitude,
        end: position.latitude
    );
    final _lngTween = Tween<double>(
        begin: _mapController.center.longitude,
        end: position.longitude
    );
    final _zoomTween = Tween<double>(begin: _mapController.zoom, end: destZoom ?? _mapController.zoom);

    // Create a animation controller that has a duration and a TickerProvider.
    AnimationController controller = AnimationController(
        duration: const Duration(milliseconds: 200),
        vsync: this
    );
    Animation<double> animation = CurvedAnimation(
        parent: controller,
        curve: Curves.fastOutSlowIn
    );

    controller.addListener(() {
      // Note that the mapController.move doesn't seem to like the zoom animation. This may be a bug in flutter_map.
      _mapController.move(
          LatLng(_latTween.evaluate(animation), _lngTween.evaluate(animation)), _zoomTween.evaluate(animation));
    });

    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.dispose();
      } else if (status == AnimationStatus.dismissed) {
        controller.dispose();
      }
    });

    controller.forward();
  }
}